package com.hdxy.xcyl.controller;


import com.hdxy.xcyl.common.annotation.Token;
import com.hdxy.xcyl.common.utils.BaseResult;
import com.hdxy.xcyl.service.YLIllnessService;
import com.hdxy.xcyl.service.YLTownShipService;
import com.hdxy.xcyl.service.YLVillageService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/ylIllness")
@Api(value="/illness",description="病情上报相关接口")
public class YLIllneController {
    private Logger logger = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private YLIllnessService ylIllnessService;
    @Autowired
    private YLTownShipService ylTownShipService;
    @Autowired
    private YLVillageService ylVillageService;
    /**
     * 查所有乡村
     * @return
     */
    @ApiOperation(value = "查所有乡村")
    @PostMapping("/selectSysAreaInfo")
    public BaseResult selectSysAreaInfo(){
        try {
            return ylIllnessService.selectYLIllneAll();
        } catch (Exception e) {
            e.printStackTrace();
            return BaseResult.fail("502", "系统异常");
        }
    }

    /**
     * 查所有病情
     * @return
     */
    @ApiOperation(value = "查所有病情")
    @PostMapping("/selectYlTownShipAll")
    public BaseResult selectYlTownShipAll(){
        try {
            return ylTownShipService.selectYLIllneAll();
        } catch (Exception e) {
            e.printStackTrace();
            return BaseResult.fail("502", "系统异常");
        }
    }

    /**
     * 病情填写提交
     */
    @Token
    @ApiOperation(value = "病情填写提交", nickname = "病情填写提交", notes = "病情填写提交", produces = "application/json")
    @PostMapping("/submitVillage")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query",name = "token", value = "token", required = true, dataType = "String"),
            @ApiImplicitParam(paramType = "query",name = "uid", value = "用户ID", required = true, dataType = "int"),
            @ApiImplicitParam(paramType = "query",name = "ylName", value = "村名姓名", required = true, dataType = "String"),
            @ApiImplicitParam(paramType = "query",name = "phone", value = "联系方式", required = true, dataType = "String"),
            @ApiImplicitParam(paramType = "query",name = "isCare", value = "是否参加合作医疗（1：是0：否)", required = true, dataType = "String"),
            @ApiImplicitParam(paramType = "query",name = "ylIllnessOne", value = "一级病情", required = true, dataType = "String"),
            @ApiImplicitParam(paramType = "query",name = "ylIllnessTwo", value = "二级病情", required = true, dataType = "String"),
            @ApiImplicitParam(paramType = "query",name = "ylVillageOne", value = "乡", required = true, dataType = "String"),
            @ApiImplicitParam(paramType = "query",name = "ylVillageTwo", value = "村", required = true, dataType = "String")
    })
    public BaseResult submitConsultation(String token, Integer uid,String ylName, String phone, String isCare, String ylIllnessOne,String ylIllnessTwo,String ylVillageOne,String ylVillageTwo) {
        try {
            BaseResult baseResult = ylVillageService.insertVillage( ylName,phone,isCare,ylIllnessOne,ylIllnessTwo,ylVillageOne,ylVillageTwo);
            return baseResult;
        } catch (Exception e) {
            e.printStackTrace();
            return BaseResult.fail("500", "系统异常");
        }

    }
}
