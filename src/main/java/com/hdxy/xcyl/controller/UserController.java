package com.hdxy.xcyl.controller;

import com.google.code.kaptcha.Producer;
import com.hdxy.xcyl.common.annotation.Token;
import com.hdxy.xcyl.common.constant.CacheConstants;
import com.hdxy.xcyl.common.constant.Constants;
import com.hdxy.xcyl.common.utils.BaseResult;
import com.hdxy.xcyl.common.utils.RedisUtil;
import com.hdxy.xcyl.common.utils.uuid.IdUtils;
import com.hdxy.xcyl.config.XmConfig;
import com.hdxy.xcyl.service.SmsService;
import com.hdxy.xcyl.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.FastByteArrayOutputStream;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.imageio.ImageIO;
import javax.naming.ldap.PagedResultsControl;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.IOException;

@RestController
@RequestMapping("/user")
@Api(value="/user",description="用户相关接口")
public class UserController {

    private Logger logger = LoggerFactory.getLogger(UserController.class);

    @Autowired
    RedisUtil redisUtil;

    @Autowired
    private UserService userService;
    @Autowired
    private SmsService smsService;

    @Resource(name = "captchaProducerMath")
    private Producer captchaProducerMath;

    @Resource(name = "captchaProducer")
    private Producer captchaProducer;

    /**
     * 发送注册验证码(小程序专用)
     */
    @ApiOperation(value = "测试注册验证码(测试专用)", nickname = "验证码", notes = "验证码", produces = "application/json")
    @GetMapping("/getKaptcha")
    @ApiImplicitParams({
//            @ApiImplicitParam(paramType = "query",name = "mobile", value = "手机号码", required = true, dataType = "String"),
    })
    public void getKaptcha(HttpServletResponse response){
        logger.info("开始生成验证码");
        // 先从redis中获取验证码
//        Object o = redisUtil.get(CacheConstants.CAPTCHA_CODE_KEY);
//        if (o != null) {
//            System.out.println(0);
//        }
        // 保存验证码信息
//        String uuid = IdUtils.simpleUUID();
//        String verifyKey = CacheConstants.CAPTCHA_CODE_KEY + uuid;
        String verifyKey = CacheConstants.CAPTCHA_CODE_KEY;

        String capStr = null, code = null;
        BufferedImage image = null;
        // 生成验证码
        String captchaType = XmConfig.getCaptchaType();
        if ("math".equals(captchaType))
        {
            String capText = captchaProducerMath.createText();
            capStr = capText.substring(0, capText.lastIndexOf("@"));
            code = capText.substring(capText.lastIndexOf("@") + 1);
            image = captchaProducerMath.createImage(capStr);
        }else if ("char".equals(captchaType))
        {
            capStr = code = captchaProducer.createText();
            image = captchaProducer.createImage(capStr);
        }
        // 在redis中保存并且设置过期时间
        boolean set = redisUtil.set(verifyKey, capStr, Constants.CAPTCHA_EXPIRATION);
        System.out.println(set);
        //将图片输出到浏览器
        response.setContentType("image/png");
        // 转换流信息写出
//        FastByteArrayOutputStream os = new FastByteArrayOutputStream();

        try
        {
            ServletOutputStream os = response.getOutputStream();
            ImageIO.write(image, "png", os);
            logger.info("验证码获取成功");
            os.close();
        }
        catch (IOException e)
        {
            logger.error("响应验证码失败：" + e.getMessage());
        }
//        return BaseResult.success(capStr);
    }

    /**
     * 发送注册验证码(小程序专用)
     */
    @ApiOperation(value = "发送注册验证码(小程序专用)", nickname = "验证码", notes = "验证码", produces = "application/json")
    @PostMapping("/sendRegisterCode")
    @ApiImplicitParams({
//            @ApiImplicitParam(paramType = "query",name = "mobile", value = "手机号码", required = true, dataType = "String"),
    })
    public BaseResult sendRegisterCode(HttpServletResponse response){
        try {
//            User user = userService.getUserByMobile(mobile);
//            if (user != null) {
//                return BaseResult.fail("502", "此用户已注册");
//            }
            BaseResult baseResult = smsService.sendSmsWithCode();
            return baseResult;
        } catch (Exception e) {
            e.printStackTrace();
            return BaseResult.fail("500", "系统异常");
        }
    }

    /**
     * 注册(小程序专用)
     */
    @ApiOperation(value = "注册(小程序专用)", nickname = "小程序专用", notes = "小程序专用", produces = "application/json")
    @PostMapping("/register")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query",name = "mobile", value = "手机号码", required = true, dataType = "String"),
            @ApiImplicitParam(paramType = "query",name = "password", value = "密码", required = true, dataType = "String"),
            @ApiImplicitParam(paramType = "query",name = "code", value = "验证码", required = false, dataType = "String"),
            @ApiImplicitParam(paramType = "query",name = "userName", value = "姓名", required = false, dataType = "String"),
            /*@ApiImplicitParam(paramType = "query",name = "imageCode", value = "图形验证码", required = true, dataType = "String"),*/
            /*@ApiImplicitParam(paramType = "query",name = "key", value = "验证码key", required = true, dataType = "String")*/
    })
    @ResponseBody
    public BaseResult register(
            String mobile,
            String password,
            String code,
           String userName,HttpServletRequest request){
        try {

            return userService.register(mobile, password, code,userName);
//            return userService.registerPC(mobile, password, code, channel, source, tradeId, inviteCode,company,userName);
        } catch (Exception e) {
            e.printStackTrace();
            return BaseResult.fail("500", "系统异常");
        }
    }

    /**
     * 登录(小程序专用)
     */
    @ApiOperation(value = "登录(小程序专用)", nickname = "登录(小程序专用)", notes = "登录(小程序专用)", produces = "application/json")
    @PostMapping("/loginAPP")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query",name = "userName", value = "用户名", required = true, dataType = "String"),
            @ApiImplicitParam(paramType = "query",name = "mobile", value = "手机号码", required = false, dataType = "String"),
            @ApiImplicitParam(paramType = "query",name = "password", value = "密码", required = true, dataType = "String")
    })
    @ResponseBody
    public BaseResult loginAPP(String userName,String mobile, String password, HttpServletRequest request){
        try {
            // 获取登录IP地址
            String ip = request.getRemoteHost();
            String sessionId = request.getSession().getId();
            System.out.println("sessionId==================="+sessionId);

            return userService.loginApp(userName, password, ip,sessionId);
        } catch (Exception e) {
            e.printStackTrace();
            return BaseResult.fail("500", "系统异常");
        }
    }

    /**
     * 修改用户信息(小程序专用)
     */
    @Token
    @ApiOperation(value = "修改用户信息(小程序专用)", nickname = "修改用户信息(小程序专用)", notes = "修改用户信息(小程序专用)", produces = "application/json")
    @PostMapping("/updateUser")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query",name = "token", value = "token", required = true, dataType = "String"),
            @ApiImplicitParam(paramType = "query",name = "uid", value = "用户ID", required = true, dataType = "int"),
            @ApiImplicitParam(paramType = "query",name = "userName", value = "用户名", required = false, dataType = "String"),
            @ApiImplicitParam(paramType = "query",name = "newPassword", value = "新密码", required = false, dataType = "String"),
            @ApiImplicitParam(paramType = "query",name = "phone", value = "电话号码", required = false, dataType = "String")
    })
    @ResponseBody
    public BaseResult updateUser(String token, Integer uid,String userName, String newPassword,String phone){
        try {
            return userService.updateUser(uid, userName, newPassword, phone );
        } catch (Exception e) {
            e.printStackTrace();
            return BaseResult.fail("500", "系统异常");
        }
    }

    /**
     * 安全退出
     */
    @ApiOperation(value = "安全退出(小程序专用)")
    @PostMapping("/exitLogin")
    @ResponseBody
    public BaseResult exitLogin(HttpServletRequest request){
        try {
            request.getSession().removeAttribute("user");
            return BaseResult.success();
        } catch (Exception e) {
            e.printStackTrace();
            return BaseResult.fail("500", "系统异常");
        }
    }

    /**
     * 验证redis
     */
    @ApiOperation(value = "验证redis", nickname = "验证redis", notes = "验证redis", produces = "application/json")
    @PostMapping("/redis")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query",name = "key", value = "key名", required = true, dataType = "String"),
            @ApiImplicitParam(paramType = "query",name = "value", value = "value值", required = false, dataType = "String")

    })
    @ResponseBody
    public BaseResult redis(String key,String value){
        try {
            boolean i = redisUtil.set(key, value);
            System.out.println(i);
            boolean set = redisUtil.set("yyy", value, 10);
            return BaseResult.success("redis缓存成功");

        } catch (Exception e) {
            e.printStackTrace();
            return BaseResult.fail("500", "系统异常");
        }
    }

}
