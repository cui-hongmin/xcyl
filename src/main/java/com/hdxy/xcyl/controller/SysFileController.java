package com.hdxy.xcyl.controller;


import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.hdxy.xcyl.common.utils.BaseResult;
import com.hdxy.xcyl.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.joda.time.DateTime;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * @Descripttion 文件上传接口
 * @Author cuihongmin
 * @Date 2023/12/7 10:53
 */

@RestController
@RequestMapping("/system/file")
@Api(value="/file",description="文件上传接口")
public class SysFileController {
    //OSS服务器访问域名
    @Value("${aliyun.oss.endpoint}")
    private String endpoint;

    //子账户名
    @Value("${aliyun.oss.accessKeyId}")
    private String accessKeyId;

    //子账户密码
    @Value("${aliyun.oss.accessKeySecret}")
    private String accessKeySecret;

    //桶名字
    @Value("${aliyun.oss.bucketName}")
    private String bucketName;

    @Autowired
    private UserService userService;


//    @PostMapping("/upload3")
//    public BaseResult upload3(@RequestParam("file") MultipartFile file) throws Exception {
//        Map<String, Object> ajax = new HashMap<>();
//        String url = AliyunOSSUtil.uploadImage(endpoint, accessKeyId, accessKeySecret, bucketName, file.getOriginalFilename(), file.getBytes());
////        System.out.println("======" + url);
//        ajax.put("url", url);
//        return R.ok(ajax);
//    }

    @PostMapping("/uploadVidoe3")
    public BaseResult uploadVidoe3(@RequestParam("file") MultipartFile file) throws Exception {
        Map<String, Object> ajax = new HashMap<>();
        // Endpoint以华北2（北京）为例，其它Region请按实际情况填写
        String endpoint = this.endpoint;
        // 阿里云账号AccessKey
        String accessKeyId = this.accessKeyId;
        String accessKeySecret = this.accessKeySecret;
        // 填写Bucket名称
        String bucketName = this.bucketName;
        try{
            // 创建OSSClient实例。
            OSS ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);
            // 上传文件流
            InputStream inputStream= file.getInputStream();
            String fileName=file.getOriginalFilename();
            String uuid = UUID.randomUUID().toString().replaceAll("-","");
            fileName = uuid+fileName;

            //按照当前日期，创建文件夹，上传到创建文件夹里面
            //  2022/03/15/xx.jpg
            String timeUrl = new DateTime().toString("yyyy/MM/dd");
            fileName = timeUrl+"/"+fileName;
            // 调用方法实现上传
            ossClient.putObject(bucketName,fileName,inputStream);

            // 关闭ossclient
            ossClient.shutdown();

            // 上传之后文件路径
            String url="https://"+bucketName+"."+endpoint+"/"+fileName;
            ajax.put("url", url);
            return BaseResult.success(ajax);
        }catch (IOException e){
            e.printStackTrace();
            return null;
        }
    }

    @ApiOperation(value="导出pdf")
    @GetMapping("/downloadPDF")
    @ApiImplicitParams({
//            @ApiImplicitParam(paramType = "query",name = "id",value = "id(中标记录id)",required = true,dataType = "int"),
    })
    public void downloadTradeBidResult( HttpServletResponse response){
        try {
            downloadPDF1(response);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void downloadPDF1(HttpServletResponse response){
        // 设置响应格式
        response.setContentType("application/pdf");
        response.setHeader("Expires","0");
        response.setHeader("Cache-Control","must-revalidate, post-check=0, pre-check=0");
        response.setHeader("Pragma","public");
        try {
        userService.createPDF();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
//        try {
            //中标记录详情
//            TradeBidRecord tradeBidRecord = (TradeBidRecord)tradeBidRecordService.getTradeBidRecordById(id).getData();
//            String fileName = tradeBidRecord.getBidId()+tradeBidRecord.getUid()+".doc";
//            String path = uploadPath+fileName;
            /*String path = "E:\\竞价结果通知单.docx";*/
            //生成word文档
//            tradeBidService.createWord(id,path);
//            File mFile = new File(path);
//            if( !mFile.exists()){
//                throw new Exception("下载路径不存在");
//            }
//            response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode(tradeBidRecord.getRowNumberStr()+"-竞价结果通知单.doc", "utf-8"));
//            OutputStream out = response.getOutputStream();
//            FileInputStream in = new FileInputStream(mFile);
//            byte[] buffer = new byte[4096];
//            int length;
//            while ((length = in.read(buffer)) > 0){
//                out.write(buffer, 0, length);
//            }
//            in.close();
//            out.flush();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
}
