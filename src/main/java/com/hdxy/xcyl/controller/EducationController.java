package com.hdxy.xcyl.controller;


import com.hdxy.xcyl.common.utils.BaseResult;
import com.hdxy.xcyl.service.EducationService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/education")
@Api(value="/education",description="预防性教育相关接口")
public class EducationController {

    private Logger logger = LoggerFactory.getLogger(UserController.class);


    @Autowired
    private EducationService educationService;


    /**
     * 获取预防性教育标题信息
     */
    @ApiOperation(value = "获取预防性教育标题信息", nickname = "获取预防性教育标题信息", notes = "获取预防性教育标题信息", produces = "application/json")
    @GetMapping("/getEducation")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query",name = "type", value = "类型(1:国家政策指南；2：防疫常识；3：疫苗接种服务)", required = true, dataType = "int"),
    })
    public BaseResult getEducation(Integer type) {
        try {
            BaseResult baseResult = educationService.selectEducation(type);
            return baseResult;
        } catch (Exception e) {
            e.printStackTrace();
            return BaseResult.fail("500", "系统异常");
        }

    }

    /**
     * 根据id获取预防性教育(国家政策指南 防疫常识)详细信息
     */
    @ApiOperation(value = "根据id获取预防性教育(国家政策指南 防疫常识)详细信息", nickname = "根据id获取预防性教育(国家政策指南 防疫常识)详细信息", notes = "根据id获取预防性教育(国家政策指南 防疫常识)详细信息", produces = "application/json")
    @GetMapping("/getEducationById")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query",name = "id", value = "id", required = true, dataType = "int"),
    })

    public BaseResult getEducationById(Integer id) {
        try {
            BaseResult baseResult = educationService.selectEducationById(id);
            return baseResult;
        } catch (Exception e) {
            e.printStackTrace();
            return BaseResult.fail("500", "系统异常");
        }

    }

    /**
     * 根据id获取预防性教育(疫苗接种服务)详细信息
     */
    @ApiOperation(value = "根据id获取预防性教育(疫苗接种服务)详细信息", nickname = "根据id获取预防性教育(疫苗接种服务)详细信息", notes = "根据id获取预防性教育(疫苗接种服务)详细信息", produces = "application/json")
    @GetMapping("/getYlSeekById")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query",name = "id", value = "id", required = true, dataType = "int"),
    })

    public BaseResult getYlSeekById(Integer id) {
        try {
            BaseResult baseResult = educationService.selectYlSeekById(id);
            return baseResult;
        } catch (Exception e) {
            e.printStackTrace();
            return BaseResult.fail("500", "系统异常");
        }

    }

}
