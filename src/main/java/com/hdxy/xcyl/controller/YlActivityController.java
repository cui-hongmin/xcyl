package com.hdxy.xcyl.controller;


import com.hdxy.xcyl.common.annotation.Token;
import com.hdxy.xcyl.common.utils.BaseResult;
import com.hdxy.xcyl.service.YlActivityService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/ylActivity")
@Api(value="/activity",description="线下医疗活动相关接口")
public class YlActivityController {

    private Logger logger = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private YlActivityService ylActivityService;


    /**
     * 获取线下医疗活动信息
     */
    @ApiOperation(value = "获取线下医疗活动信息", nickname = "获取线下医疗活动信息", notes = "获取线下医疗活动信息", produces = "application/json")
    @GetMapping("/getActivityList")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query",name = "page", value = "page", required = true, dataType = "int"),
            @ApiImplicitParam(paramType = "query",name = "rows", value = "rows", required = true, dataType = "int")
    })
    public BaseResult getActivityList(Integer page,Integer rows) {
        try {
            BaseResult baseResult = ylActivityService.selectActivityList(page,rows);
            return baseResult;
        } catch (Exception e) {
            e.printStackTrace();
            return BaseResult.fail("500", "系统异常");
        }

    }

    /**
     * 根据用户名获取线下医疗活动结果
     */
    @Token
    @ApiOperation(value = "根据用户名获取线下医疗活动结果", nickname = "根据用户名获取线下医疗活动结果", notes = "根据用户名获取线下医疗活动结果", produces = "application/json")
    @GetMapping("/getMyActivityList")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query",name = "uid", value = "用户ID", required = true, dataType = "int"),
            @ApiImplicitParam(paramType = "query",name = "token", value = "token", required = true, dataType = "String"),
            @ApiImplicitParam(paramType = "query",name = "userName", value = "用户名", required = true, dataType = "String"),
            @ApiImplicitParam(paramType = "query",name = "page", value = "page", required = true, dataType = "int"),
            @ApiImplicitParam(paramType = "query",name = "rows", value = "rows", required = true, dataType = "int")
    })
    public BaseResult getMyActivityList(Integer uid,String token,String userName,Integer page,Integer rows) {
        try {
            BaseResult baseResult = ylActivityService.selectMyActivityList(uid,userName,page,rows);
            return baseResult;
        } catch (Exception e) {
            e.printStackTrace();
            return BaseResult.fail("500", "系统异常");
        }

    }
}
