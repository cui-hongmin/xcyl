package com.hdxy.xcyl.controller;

import com.hdxy.xcyl.common.utils.BaseResult;
import com.hdxy.xcyl.entity.Student;
import com.hdxy.xcyl.mapper.StudentMapper;
import com.hdxy.xcyl.service.PessimisticLockService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.ibatis.session.ExecutorType;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CountDownLatch;

@RestController
@RequestMapping("/learn")
@Api(value="/learn",description="学习相关接口")
public class PlcrTestController {

    @Autowired
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;
    
    @Autowired
    private PlatformTransactionManager transactionManager;

    @Autowired
    private StudentMapper studentMapper;

    @Autowired
    private PessimisticLockService pessimisticLockService;

    @Resource
    private SqlSessionFactory sqlSessionFactory;

    private Logger logger = LoggerFactory.getLogger(UserController.class);


    /**
     * ThreadPoolTaskExecutor 验证批量新增
     */
    @ApiOperation(value = "验证批量新增", nickname = "验证批量新增", notes = "验证批量新增", produces = "application/json")
    @GetMapping("/batchInsert2")
    @ApiImplicitParams({
    })
    public void batchInsert2() {
        ArrayList<Student> arrayList = new ArrayList<>();
        long startTime = System.currentTimeMillis();
        System.out.println(startTime);
        // 模拟数据 创建50000个学生对象
        for (int i = 0; i < 50000; i++) {
            Student student = new Student("李明" + i, 20, "张家界市" + i, i + "号");
            arrayList.add(student);
        }
        int count = arrayList.size();
        int pageSize = 1000;  // 每批次插入的数据量
        int threadNum = count / pageSize + 1;  // 线程数量
        CountDownLatch countDownLatch = new CountDownLatch(threadNum);
        for (int i = 0; i < threadNum; i++) {
            int startIndex = i * pageSize;
            int endIndex = Math.min(count, (i + 1) * pageSize);
            List<Student> subList = arrayList.subList(startIndex, endIndex);
            threadPoolTaskExecutor.execute(() -> {
                DefaultTransactionDefinition transactionDefinition = new DefaultTransactionDefinition();
                TransactionStatus status = transactionManager.getTransaction(transactionDefinition);
                try {
                    studentMapper.insertSplice(subList);
                    transactionManager.commit(status);
                } catch (Exception e) {
                    transactionManager.rollback(status);
                    throw e;
                } finally {
                    countDownLatch.countDown();
                }
            });
        }
        try {
            countDownLatch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        long endTime = System.currentTimeMillis();
        System.out.println(endTime);
    }


    /**
     * 使用sqlSessionFactory实现批量插入
     */

    @ApiOperation(value = "使用sqlSessionFactory验证批量新增", nickname = "使用sqlSessionFactory验证批量新增", notes = "使用sqlSessionFactory验证批量新增", produces = "application/json")
    @GetMapping("/batchInsert3")
    @ApiImplicitParams({
    })
    public BaseResult batchInsert3() {
        ArrayList<Student> arrayList = new ArrayList<>();
        long start = System.currentTimeMillis();
        // 模拟数据 创建50000个学生对象
        for (int i = 0; i < 50000; i++) {
            Student student = new Student("李明" + i, 20, "张家界市" + i, i + "号");
            arrayList.add(student);
        }
        // 指定分页大小
        int pageSize = 1000; // 每批插入1000条数据
        // 关闭session的自动提交
        SqlSession sqlSession = sqlSessionFactory.openSession(ExecutorType.BATCH, false);
        try {
            StudentMapper bankMapper = sqlSession.getMapper(StudentMapper.class);
            // 计算总页数
            int totalSize = arrayList.size();
            int totalPages = (int) Math.ceil((double) totalSize / pageSize);
            for (int page = 1; page <= totalPages; page++) {
                // 计算当前页的起始和结束索引
                int startIndex = (page - 1) * pageSize;
                int endIndex = Math.min(startIndex + pageSize, totalSize);

                // 获取当前页的数据
                List<Student> banks = arrayList.subList(startIndex, endIndex);
                // 批量插入数据
                studentMapper.insertSplice(banks);
                // 提交事务
                sqlSession.commit();
            }
//            msg="恭喜您，数据已全部导入成功！";
        } catch (Exception e) {
            sqlSession.rollback();
            logger.error(e.getMessage());
        } finally {
            sqlSession.close();
        }
        long end = System.currentTimeMillis();
        logger.info("数据总耗时：" + (end-start) + "ms" );
        return BaseResult.success("恭喜您，数据已全部导入成功！数据总耗时：" + (end-start) + "ms");
    }

    /**
     *
     *乐观锁  方式一：使用剩余量即版本号
     * @return
     *
     */
    @ApiOperation(value = "乐观锁  方式一：使用剩余量即版本号", nickname = "乐观锁  方式一：使用剩余量即版本号", notes = "乐观锁  方式一：使用剩余量即版本号", produces = "application/json")
    @GetMapping("/optimisticLock")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query",name = "id", value = "产品id主键", required = true, dataType = "int"),
            @ApiImplicitParam(paramType = "query",name = "purchaseNumber", value = "购买数量", required = true, dataType = "BigDecimal"),
//            @ApiImplicitParam(paramType = "query",name = "userName", value = "用户名", required = false, dataType = "String"),
    })
    public BaseResult optimisticLock(Integer id, BigDecimal purchaseNumber) {
            try {
                BaseResult baseResult = pessimisticLockService.
                        insertOrder(id,purchaseNumber);
                return baseResult;
            } catch (Exception e) {
                e.printStackTrace();
                return BaseResult.fail("500", "系统异常");
            }

        }

    /**
     *
     *悲观锁  方式一：使用Lock接口
     * @return
     *
     */
    @ApiOperation(value = "悲观锁  方式一：使用Lock接口", nickname = "悲观锁  方式一：使用Lock接口", notes = "悲观锁  方式一：使用Lock接口", produces = "application/json")
    @GetMapping("/pessimisticLock")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query",name = "id", value = "产品id主键", required = true, dataType = "int"),
            @ApiImplicitParam(paramType = "query",name = "purchaseNumber", value = "购买数量", required = true, dataType = "BigDecimal"),
//            @ApiImplicitParam(paramType = "query",name = "userName", value = "用户名", required = false, dataType = "String"),
    })
    public BaseResult pessimisticLock(Integer id, BigDecimal purchaseNumber) {
        try {
            synchronized (this) {
                BaseResult baseResult = pessimisticLockService.
                        insertOrder1(id,purchaseNumber);
                return baseResult;
            }

        } catch (Exception e) {
            e.printStackTrace();
            return BaseResult.fail("500", "系统异常");
        }

    }

    public static void main(String[] args) throws Exception {
        String time="2023-04-18";
        Date date=null;
        SimpleDateFormat formatter=new SimpleDateFormat("yyyy-MM-dd");
        date=formatter.parse(time);


        SimpleDateFormat ft = new SimpleDateFormat ("yyyy-MM-dd");
        Date startTime = ft.parse("2022-04-18");
        Date endTime = ft.parse("2022-04-19");

        Date nowTime = date;

        boolean effectiveDate = isEffectiveDate(nowTime, startTime, endTime);
        if (effectiveDate) {
            System.out.println("当前时间在范围内");
        }else {
            System.out.println("当前时间在不在范围内");
        }
    }

    /**
     *
     * @param nowTime   当前时间
     * @param startTime  开始时间
     * @param endTime   结束时间
     * @return
     * @author sunran   判断当前时间在时间区间内
     */
    public static boolean isEffectiveDate(Date nowTime, Date startTime, Date endTime) {
        if (nowTime.getTime() == startTime.getTime()
                || nowTime.getTime() == endTime.getTime()) {
            return true;
        }

        Calendar date = Calendar.getInstance();
        date.setTime(nowTime);

        Calendar begin = Calendar.getInstance();
        begin.setTime(startTime);

        Calendar end = Calendar.getInstance();
        end.setTime(endTime);

        if (date.after(begin) && date.before(end)) {
            return true;
        } else {
            return false;
        }
    }



}
