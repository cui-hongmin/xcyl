package com.hdxy.xcyl.controller;

import com.hdxy.xcyl.common.annotation.Token;
import com.hdxy.xcyl.common.utils.BaseResult;
import com.hdxy.xcyl.entity.YlInfusion;
import com.hdxy.xcyl.service.YlDoctorService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/consultation")
@Api(value="/consultation",description="线上问诊、线上预约输液及问诊查询相关接口")
public class ConsultationController {

    private Logger logger = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private YlDoctorService ylDoctorService;



    /**
     * 获取医生信息
     */
    @ApiOperation(value = "获取医生信息列表", nickname = "获取医生信息列表", notes = "获取医生信息列表", produces = "application/json")
    @GetMapping("/getDoctorList")
    @ApiImplicitParams({
//            @ApiImplicitParam(paramType = "query",name = "type", value = "类型(1:国家政策指南；2：防疫常识；3：疫苗接种服务)", required = true, dataType = "int"),
    })
    public BaseResult getDoctorList() {
        try {
            BaseResult baseResult = ylDoctorService.selectDoctorList();
            return baseResult;
        } catch (Exception e) {
            e.printStackTrace();
            return BaseResult.fail("500", "系统异常");
        }

    }

    /**
     * 线上问诊提交
     */
    @Token
    @ApiOperation(value = "线上问诊提交", nickname = "线上问诊提交", notes = "线上问诊提交", produces = "application/json")
    @PostMapping("/submitConsultation")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query",name = "token", value = "token", required = true, dataType = "String"),
            @ApiImplicitParam(paramType = "query",name = "uid", value = "用户ID", required = true, dataType = "int"),
            @ApiImplicitParam(paramType = "query",name = "ysName", value = "医生姓名", required = true, dataType = "String"),
            @ApiImplicitParam(paramType = "query",name = "doctorNumber", value = "医生编号", required = true, dataType = "String"),
            @ApiImplicitParam(paramType = "query",name = "userName", value = "用户姓名", required = true, dataType = "String"),
            @ApiImplicitParam(paramType = "query",name = "userSymptoms", value = "用户病症", required = true, dataType = "String")
    })
    public BaseResult submitConsultation(String token, Integer uid, String ysName,Integer doctorNumber, String userName, String userSymptoms) {
        try {
            BaseResult baseResult = ylDoctorService.insertConsultation(uid, ysName,doctorNumber,userName,userSymptoms);
            return baseResult;
        } catch (Exception e) {
            e.printStackTrace();
            return BaseResult.fail("500", "系统异常");
        }

    }

    /**
     * 线上预约输液提交
     */
    @Token
    @ApiOperation(value = "线上预约输液提交", nickname = "线上预约输液提交", notes = "线上预约输液提交", produces = "application/json")
    @PostMapping("/submitInfusion")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query",name = "token", value = "token", required = true, dataType = "String"),
            @ApiImplicitParam(paramType = "query",name = "uid", value = "用户ID", required = true, dataType = "int"),
            @ApiImplicitParam(paramType = "query",name = "userName", value = "用户姓名", required = true, dataType = "String"),
            @ApiImplicitParam(paramType = "query",name = "userAge", value = "用户年龄", required = true, dataType = "int"),
            @ApiImplicitParam(paramType = "query",name = "userSex", value = "用户性别（1：男；0：女）", required = true, dataType = "int"),
            @ApiImplicitParam(paramType = "query",name = "userMobile", value = "用户联系方式", required = true, dataType = "String"),
            @ApiImplicitParam(paramType = "query",name = "userAddress", value = "用户详细地址", required = true, dataType = "String"),
            @ApiImplicitParam(paramType = "query",name = "userDescribe", value = "用户病情描述", required = true, dataType = "String")
    })
    public BaseResult submitInfusion(String token, Integer uid, String userName, Integer userAge,Integer userSex, String userMobile, String userAddress, String userDescribe) {
        try {
            BaseResult baseResult = ylDoctorService.insertInfusion(uid, userName, userAge, userSex, userMobile, userAddress, userDescribe);
            return baseResult;
        } catch (Exception e) {
            e.printStackTrace();
            return BaseResult.fail("500", "系统异常");
        }

    }

    /**
     * 问诊查询
     */
    @Token
    @ApiOperation(value = "获取问诊查询信息列表", nickname = "获取问诊查询信息列表", notes = "获取问诊查询信息列表", produces = "application/json")
    @GetMapping("/getSymptomsList")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query",name = "token", value = "token", required = true, dataType = "String"),
            @ApiImplicitParam(paramType = "query",name = "uid", value = "用户ID", required = true, dataType = "int")
    })
    public BaseResult getSymptomsList(String token, Integer uid) {
        try {
            BaseResult baseResult = ylDoctorService.selectSymptomsList(uid);
            return baseResult;
        } catch (Exception e) {
            e.printStackTrace();
            return BaseResult.fail("500", "系统异常");
        }

    }




}
