package com.hdxy.xcyl.Stream.functionobject;

import java.util.ArrayList;
import java.util.List;

//函数对象的好处1：行为参数化
public class Sample1 {
    public static void main(String[] args) {
        List<Student> students = new ArrayList<>();
        students.add(new Student("张三", 18, "男"));
        students.add(new Student("杨不诲", 20, "女"));
        students.add(new Student("周芷若",16, "女"));
        students.add(new Student("宋情书",21, "男"));
//        List<Student> students = List.of(
//                new Student("张三", 18, "男"),
//                new Student("杨不诲", 20, "女"),
//                new Student("周芷若",19, "女"),
//                new Student("宋情书",21, "男")
//
//        );
        /**需求1 筛选男性学生*/
        System.out.println(filter0(students,student -> student.sex.equals("男")));
        /**需求2 筛选18岁以下的学生*/
        System.out.println(filter0(students,student -> student.age<18));
    }





    interface  Lambda{
        boolean test(Student student);
    }
    static List<Student> filter0(List<Student> students,Lambda lambda)  {
        List<Student> result = new ArrayList<>();
        for (Student student : students) {
            if (lambda.test(student)) {
                result.add(student);
            }
        }
        return result;
    }
    // 函数对象格式 参数 -> 逻辑部分 student -> student.sex.equals("男")
  static List<Student> filter(List<Student> students) {
      List<Student> result = new ArrayList<>();
      for (Student student : students) {
          if (student.getSex().equals("男")) {
              result.add(student);
          }
      }
      return result;
  }

    // 函数对象格式 参数 -> 逻辑部分 student -> student.age<18
    static List<Student> filter2(List<Student> students) {
        List<Student> result = new ArrayList<>();
        for (Student student : students) {
            if (student.getAge()<18) {
                result.add(student);
            }
        }
        return result;
    }

    static class Student{
        private String name;
        private Integer age;
        private String sex;

        public Student(String name, Integer age, String sex) {
            this.name = name;
            this.age = age;
            this.sex = sex;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Integer getAge() {
            return age;
        }

        public void setAge(Integer age) {
            this.age = age;
        }

        public String getSex() {
            return sex;
        }

        public void setSex(String sex) {
            this.sex = sex;
        }

        @Override
        public String toString() {
            return "Student{" +
                    "name='" + name + '\'' +
                    ", age=" + age +
                    ", sex='" + sex + '\'' +
                    '}';
        }
    }

}
