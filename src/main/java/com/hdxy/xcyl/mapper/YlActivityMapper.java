package com.hdxy.xcyl.mapper;

import com.hdxy.xcyl.entity.YlActivity;
import com.hdxy.xcyl.entity.YlSeek;

import java.util.List;
import java.util.Map;

public interface YlActivityMapper {

    List<YlActivity> selectYlActivityList(Map<String,Object> param);

    int selectActivityCount();

    //将库里数据设置【进行中】
    int updateByTime();

    //将库里数据设置【已结束】
    int updateByTime1();
}
