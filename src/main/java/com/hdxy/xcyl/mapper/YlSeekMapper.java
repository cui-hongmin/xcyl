package com.hdxy.xcyl.mapper;

import com.hdxy.xcyl.entity.YlSeek;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface YlSeekMapper {

    List<YlSeek> selectYlSeekList();

    YlSeek selectYlSeekById(@Param("id")Integer id);
}
