package com.hdxy.xcyl.mapper;

import com.hdxy.xcyl.entity.YLVillage;
import com.hdxy.xcyl.entity.YlRecord;

public interface YLVillageMapper {

    /**
     * 新增病情填写信息
     *
     * @param ylVillage 病情填写信息
     * @return 结果
     */
    public boolean insertYlVillage(YLVillage ylVillage);

}
