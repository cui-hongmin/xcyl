package com.hdxy.xcyl.mapper;

import com.hdxy.xcyl.entity.Order;
import com.hdxy.xcyl.entity.User;

import java.util.List;

public interface OrderMapper {

    /**
     * 获取订单信息列表
     * */
    List<Order> selectOrderList();

    /**
     * 新增订单
     * */
    int insertOrder(Order order);
}
