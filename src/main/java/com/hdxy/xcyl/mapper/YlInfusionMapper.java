package com.hdxy.xcyl.mapper;

import com.hdxy.xcyl.entity.YlInfusion;
import com.hdxy.xcyl.entity.YlSymptoms;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface YlInfusionMapper {

    int insertYlInfusion(YlInfusion ylInfusion);

    /**
     * 查询最新的一条数据
     * */
    YlInfusion selectYlInfusionNow();


}
