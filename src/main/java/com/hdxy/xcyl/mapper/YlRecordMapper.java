package com.hdxy.xcyl.mapper;

import com.hdxy.xcyl.entity.YlActivity;
import com.hdxy.xcyl.entity.YlRecord;

import java.util.List;
import java.util.Map;

public interface YlRecordMapper {
    /**
     * 根据uid查询我的医疗下乡结果
     *
     * @return 结果
     */
    List<YlRecord> selectYlRecordList(Map<String,Object> param);

    int selectYlRecordCount(Map<String,Object> param);

    /**
     * 新增医疗下乡记录信息
     *
     * @param ylRecord 医疗下乡记录信息
     * @return 结果
     */
    public boolean insertYlRecord(YlRecord ylRecord);


    /**
     * 通知医疗结果信息
     *
     * @return
     */
    public int updateRecordById(YlRecord ylRecord);
}
