package com.hdxy.xcyl.mapper;

import com.hdxy.xcyl.entity.Product;
import com.hdxy.xcyl.entity.User;
import com.hdxy.xcyl.entity.YlDoctor;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;

public interface ProductMapper {

    /**
     * 获取商品信息列表
     * */
    List<Product> selectProductList();

    Product selectProductById(@Param("id") Integer id);

    /**
     * 修改剩余量
     * */
    int updateProductResidue(@Param("productId") Integer productId, @Param("purchaseRedNumber") BigDecimal purchaseRedNumber);

    int updateProductResidue1(@Param("productId") Integer productId, @Param("purchaseRedNumber") BigDecimal purchaseRedNumber);
}
