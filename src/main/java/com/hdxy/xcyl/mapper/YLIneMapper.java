package com.hdxy.xcyl.mapper;

import com.hdxy.xcyl.entity.YLIllne;
import com.hdxy.xcyl.entity.YlDoctor;

import java.util.List;

public interface YLIneMapper {

    // 获取所有乡村下拉字典
    List<YLIllne> selectYLIllneList();

    /**
     * 根据一级乡的areaCode 查children
     * @param areaCode
     * @return
     */
    List<YLIllne> selectSysAreaInfoByAreaCode(Integer areaCode);
}
