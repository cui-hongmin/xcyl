package com.hdxy.xcyl.mapper;

import com.hdxy.xcyl.entity.Education;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface EductationMapper {

    List<Education> selectEducationList(@Param("type")Integer type);

    Education selectEducationById(@Param("id")Integer id);
}
