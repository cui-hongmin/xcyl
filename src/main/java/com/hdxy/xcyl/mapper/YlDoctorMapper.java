package com.hdxy.xcyl.mapper;

import com.hdxy.xcyl.entity.YlDoctor;

import java.util.List;

public interface YlDoctorMapper {

    /**
     * 获取医生信息列表
     * */
    List<YlDoctor> selectYlDoctorList();


}
