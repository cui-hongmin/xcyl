package com.hdxy.xcyl.mapper;

import com.hdxy.xcyl.entity.YlInfusion;
import com.hdxy.xcyl.entity.YlSymptoms;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface YlSymptomsMapper {

    int insertYlSymptoms(YlSymptoms ylSymptoms);

    List<YlSymptoms> selectSymptomsList(@Param("uid")Integer uid);
}
