package com.hdxy.xcyl.mapper;

import com.hdxy.xcyl.entity.User;
import com.hdxy.xcyl.entity.UserLoginLogs;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

public interface UserMapper {
    User selectUserById(Integer id);

    int insertUser(User user);

    int updateUserById(User userMap);

    /**
     * 通过User的mobile获得User对象
     * @param mobile
     * @return
     */
    User selectUserByMobile(@Param("mobile") String mobile);

    /**
     * 通过User的userName获得User对象
     * @param userName
     * @return
     */
    User selectUserByUserName(@Param("userName") String userName);

    /**
     * 通过User的uid获得User对象
     * @param uid
     * @return
     */
    User selectUserByUid(@Param("uid") Integer uid);


    /**
     * 插入UserLoginLogs到数据库,包括null值
     * @param value
     * @return
     */
    int insertUserLoginLogs(UserLoginLogs value);
}
