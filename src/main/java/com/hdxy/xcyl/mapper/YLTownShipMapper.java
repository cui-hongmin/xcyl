package com.hdxy.xcyl.mapper;

import com.hdxy.xcyl.entity.YLIllne;
import com.hdxy.xcyl.entity.YLTownShip;

import java.util.List;

public interface YLTownShipMapper {
    // 获取所有病情下拉字典
    List<YLTownShip> selectYLIllneList();

    /**
     * 根据一级病情的areaCode 查children
     * @param areaCode
     * @return
     */
    List<YLTownShip> selectSysAreaInfoByAreaCode(Integer areaCode);
}
