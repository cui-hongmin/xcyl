package com.hdxy.xcyl.common.utils;

import java.util.regex.Pattern;

public class CommonUtils {

    public static final String PhoneNumPattern = "1[23456789]\\d{9}";
    /**
     * Description:判断字符是否为""、null、" "
     * @param character 需要检验的字符串
     * @return ""、null、" "等字符串返回true；否则返回false
     * @author xianlong@huaxiafinance.com
     * @date 2015年8月27日 上午1:07:14
     */
    public static boolean isEmpty(String character) {
        int strLen;
        if (character == null || (strLen = character.length()) == 0 || "null".equals(character)) {
            return true;
        }
        for (int i = 0; i < strLen; i++) {
            if ((Character.isWhitespace(character.charAt(i)) == false)) {
                return false;
            }
        }
        return true;
    }
    /**
     * Description: 检查手机号码，空值或者不合正则表达式的均返回false
     * @param mobile  需要检查的号码
     * @return boolean 是手机号为true,否则false
     * @author xianlong@huaxiafinance.com
     * @date 2015年8月27日 上午10:10:42
     */
    public static boolean isMobile(String mobile) {
        if (isEmpty(mobile))
            return false;
        Pattern mobilePattern = Pattern.compile(PhoneNumPattern);
        if (!mobilePattern.matcher(mobile).find()) {
            return false;
        } else if (mobile.length() != 11) {
            return false;
        }
        return true;
    }
}
