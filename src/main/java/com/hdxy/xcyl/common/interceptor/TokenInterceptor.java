package com.hdxy.xcyl.common.interceptor;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hdxy.xcyl.common.annotation.Token;
import com.hdxy.xcyl.common.utils.BaseResult;
import com.hdxy.xcyl.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Method;

@Component
public class TokenInterceptor implements HandlerInterceptor {
    private static Logger log = LoggerFactory.getLogger(TokenInterceptor.class);

    @Autowired
    private UserService userService;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        try {
            if (handler instanceof HandlerMethod) {
                HandlerMethod handlerMethod = (HandlerMethod) handler;
                Method method = handlerMethod.getMethod();
                Token annotation = method.getAnnotation(Token.class);
                if (null != annotation) {
                    if (annotation.isToken()) {
                        String uid = request.getParameter("uid");
                        String token = request.getParameter("token");
    //						if(uid==null || "".equals(uid)){
    //							return returnFalse(response,null, "uid Can not be empty");
    //						}
    //						if(token==null || "".equals(token)){
    //							return returnFalse(response, null,"token Can not be empty");
    //						}
                        BaseResult result = userService.checkUserAuth(token, uid);
                        if (result.isSuccess()) {
                            return true;
                        } else {
                            return returnFalse(response, result, result.getErrorMsg());
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    private boolean returnFalse(HttpServletResponse response,BaseResult base,String message) {
        try {
            if(base==null){
                base = new BaseResult();
            }
            PrintWriter out = response.getWriter();
            response.setCharacterEncoding("UTF-8");
            response.setHeader("Content-type", "text/html;charset=UTF-8");
            response.setContentType("application/json; charset=utf-8");
            base.setErrorMsg(message);
            out.print(new ObjectMapper().writeValueAsString(base));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }
}
