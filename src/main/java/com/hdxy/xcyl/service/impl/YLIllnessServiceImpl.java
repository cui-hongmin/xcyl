package com.hdxy.xcyl.service.impl;

import com.hdxy.xcyl.common.utils.BaseResult;
import com.hdxy.xcyl.common.utils.RedisUtil;
import com.hdxy.xcyl.entity.YLIllne;
import com.hdxy.xcyl.mapper.YLIneMapper;
import com.hdxy.xcyl.service.YLIllnessService;
import com.alibaba.fastjson.JSON;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class YLIllnessServiceImpl implements YLIllnessService {
    private static final String SYS_AREA = "yl_area_info";
    @Autowired
    RedisUtil redisUtil;
    @Autowired
    private YLIneMapper ylIneMapper;


    @Override
    public BaseResult selectYLIllneAll() {
        List<YLIllne> sysAreaInfoList = null;
        Object object = redisUtil.get(SYS_AREA);
        if(object==null || "".equals(object)) {
            sysAreaInfoList = ylIneMapper.selectYLIllneList();
            getChildList(sysAreaInfoList);
            redisUtil.set(SYS_AREA,JSON.toJSON(sysAreaInfoList).toString());
        }else {
            sysAreaInfoList = JSON.parseArray(object.toString(), YLIllne.class);
        }
        return BaseResult.success(sysAreaInfoList);
    }

    public List<YLIllne> getChildList (List<YLIllne> sysAreaInfoList){
        for (YLIllne sysAreaInfo : sysAreaInfoList) {
            List<YLIllne> childList = ylIneMapper.selectSysAreaInfoByAreaCode(sysAreaInfo.getId());
            sysAreaInfo.setChildren(childList);
            getChildList(childList);
        }
        return sysAreaInfoList;
    }
}
