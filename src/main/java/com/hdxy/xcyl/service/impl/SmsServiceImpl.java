package com.hdxy.xcyl.service.impl;

import com.google.code.kaptcha.Producer;
import com.hdxy.xcyl.common.constant.CacheConstants;
import com.hdxy.xcyl.common.constant.Constants;
import com.hdxy.xcyl.common.utils.BaseResult;
import com.hdxy.xcyl.common.utils.RedisUtil;
import com.hdxy.xcyl.common.utils.sign.Base64;
import com.hdxy.xcyl.common.utils.uuid.IdUtils;
import com.hdxy.xcyl.config.XmConfig;
import com.hdxy.xcyl.service.SmsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.FastByteArrayOutputStream;

import javax.annotation.Resource;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Service
public class SmsServiceImpl implements SmsService {

    @Autowired
    RedisUtil redisUtil;
    @Resource(name = "captchaProducerMath")
    private Producer captchaProducerMath;

    @Resource(name = "captchaProducer")
    private Producer captchaProducer;



    @Override
    public BaseResult checkCode(String mobile, String tmpKey, String code) {
        String verifyKey = CacheConstants.CAPTCHA_CODE_KEY;
//        String redisCode = (String) redisUtil.get("code_" + tmpKey + "_"
//                + mobile);
        String redisCode = (String) redisUtil.get(verifyKey);
        if (redisCode == null) {
            return BaseResult.fail("001", "验证码验证失败");
        }
        if (!code.equals(redisCode)) {
            return BaseResult.fail("001", "验证码验证失败");
        }
        return BaseResult.success(null);
    }

    @Override
    public BaseResult sendSmsWithCode() {
        BaseResult baseResult = new BaseResult();
        // 先从redis中获取验证码
        // 保存验证码信息
        String uuid = IdUtils.simpleUUID();
        String verifyKey = CacheConstants.CAPTCHA_CODE_KEY;

        String capStr = null, code = null;
        BufferedImage image = null;
        // 生成验证码
        String captchaType = XmConfig.getCaptchaType();
        if ("math".equals(captchaType))
        {
            String capText = captchaProducerMath.createText();
            capStr = capText.substring(0, capText.lastIndexOf("@"));
            code = capText.substring(capText.lastIndexOf("@") + 1);
            image = captchaProducerMath.createImage(capStr);
        }else if ("char".equals(captchaType))
        {
            capStr = code = captchaProducer.createText();
            image = captchaProducer.createImage(capStr);
        }
        // 在redis中保存并且设置过期时间
        redisUtil.set(verifyKey, capStr, Constants.CAPTCHA_EXPIRATION);
        // 转换流信息写出
        FastByteArrayOutputStream os = new FastByteArrayOutputStream();
        try
        {
            ImageIO.write(image, "jpg", os);
        }
        catch (IOException e)
        {
            return BaseResult.fail("201",e.getMessage());
        }
        Map map = new HashMap();
        map.put("uuid", uuid);
        map.put("img", Base64.encode(os.toByteArray()));
        map.put("code",capStr);
        baseResult.setData(map);
//        baseResult.setTotal(pageInfo.getTotal());
//        baseResult.setTotalPage(pageInfo.getPages());
        baseResult.setSuccess(true);
        return baseResult;
    }

    //    @Override
//    public boolean selectCaptchaEnabled() {
//        String captchaEnabled = selectConfigByKey("sys.account.captchaEnabled");
//        if (StringUtil.isEmpty(captchaEnabled))
//        {
//            return true;
//        }
//        return Convert.toBool(captchaEnabled);
//    }
}
