package com.hdxy.xcyl.service.impl;

import com.hdxy.xcyl.common.utils.BaseResult;
import com.hdxy.xcyl.entity.Education;
import com.hdxy.xcyl.entity.YlSeek;
import com.hdxy.xcyl.mapper.EductationMapper;
import com.hdxy.xcyl.mapper.YlSeekMapper;
import com.hdxy.xcyl.service.EducationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EducationServiceImpl implements EducationService {

    @Autowired
    private EductationMapper eductationMapper;

    @Autowired
    private YlSeekMapper ylSeekMapper;

    @Override
    public BaseResult selectEducation(Integer type) {
        if (type != null) {
            if (type == 3) {
                // 查询以疫苗常识的标题信息
                List<YlSeek> ylSeeks = ylSeekMapper.selectYlSeekList();
                return BaseResult.success(ylSeeks);

            }else {
                // 查询国家政策、医疗常识标题信息
                List<Education> educations = eductationMapper.selectEducationList(type);
                return BaseResult.success(educations);
            }

        }
        return BaseResult.fail("502","参数传递错误！");
    }

    @Override
    public BaseResult selectEducationById(Integer id) {
        Education education = eductationMapper.selectEducationById(id);
        return BaseResult.success(education);
    }

    @Override
    public BaseResult selectYlSeekById(Integer id) {
        YlSeek ylSeek = ylSeekMapper.selectYlSeekById(id);
        return BaseResult.success(ylSeek);
    }
}
