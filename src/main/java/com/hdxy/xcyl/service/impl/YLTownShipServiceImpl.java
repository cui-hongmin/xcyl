package com.hdxy.xcyl.service.impl;

import com.alibaba.fastjson.JSON;
import com.hdxy.xcyl.common.utils.BaseResult;
import com.hdxy.xcyl.common.utils.RedisUtil;
import com.hdxy.xcyl.entity.YLIllne;
import com.hdxy.xcyl.entity.YLTownShip;
import com.hdxy.xcyl.mapper.YLIneMapper;
import com.hdxy.xcyl.mapper.YLTownShipMapper;
import com.hdxy.xcyl.service.YLTownShipService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class YLTownShipServiceImpl implements YLTownShipService {

    private static final String SYS_AREA = "yl_Town_ship";
    @Autowired
    RedisUtil redisUtil;
    @Autowired
    private YLTownShipMapper ylTownShipMapper;


    @Override
    public BaseResult selectYLIllneAll() {
        List<YLTownShip> sysAreaInfoList = null;
        Object object = redisUtil.get(SYS_AREA);
        if(object==null || "".equals(object)) {
            sysAreaInfoList = ylTownShipMapper.selectYLIllneList();
            getChildList(sysAreaInfoList);
            redisUtil.set(SYS_AREA, JSON.toJSON(sysAreaInfoList).toString());
        }else {
            sysAreaInfoList = JSON.parseArray(object.toString(), YLTownShip.class);
        }
        return BaseResult.success(sysAreaInfoList);
    }

    public List<YLTownShip> getChildList (List<YLTownShip> sysAreaInfoList){
        for (YLTownShip sysAreaInfo : sysAreaInfoList) {
            List<YLTownShip> childList = ylTownShipMapper.selectSysAreaInfoByAreaCode(sysAreaInfo.getId());
            sysAreaInfo.setChildren(childList);
            getChildList(childList);
        }
        return sysAreaInfoList;
    }
}
