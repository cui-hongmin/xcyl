package com.hdxy.xcyl.service.impl;

import com.hdxy.xcyl.common.utils.BaseResult;
import com.hdxy.xcyl.entity.YLVillage;
import com.hdxy.xcyl.entity.YlSymptoms;
import com.hdxy.xcyl.mapper.YLVillageMapper;
import com.hdxy.xcyl.service.YLVillageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class YLVillageServiceImpl implements YLVillageService {

    @Autowired
    private YLVillageMapper ylVillageMapper;

    @Override
    public BaseResult insertVillage(String ylName, String phone, String isCare, String ylIllnessOne, String ylIllnessTwo, String ylVillageOne, String ylVillageTwo) {
        BaseResult base = new BaseResult();
        YLVillage ylVillage = new YLVillage();
        ylVillage.setYlName(ylName);
        ylVillage.setPhone(phone);
        ylVillage.setIsCare(isCare);
        ylVillage.setYlIllnessOne(ylIllnessOne);
        ylVillage.setYlIllnessTwo(ylIllnessTwo);
        ylVillage.setYlVillageOne(ylVillageOne);
        ylVillage.setYlVillageTwo(ylVillageTwo);
        ylVillage.setCreateTime(new Date());

        Boolean i = ylVillageMapper.insertYlVillage(ylVillage);
//        int count = shopOrderMapper.insertShopOrder(shopOrder);
        if (i) {
            return BaseResult.success();
        }
        return BaseResult.fail("502","新增数据失败");
    }
}
