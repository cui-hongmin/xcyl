package com.hdxy.xcyl.service.impl;

import com.hdxy.xcyl.common.utils.BaseResult;
import com.hdxy.xcyl.entity.YlActivity;
import com.hdxy.xcyl.entity.YlRecord;
import com.hdxy.xcyl.mapper.YlActivityMapper;
import com.hdxy.xcyl.mapper.YlRecordMapper;
import com.hdxy.xcyl.service.YlActivityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class YlActivityServiceImpl implements YlActivityService {

    @Autowired
    private YlActivityMapper ylActivityMapper;
    @Autowired
    private YlRecordMapper ylRecordMapper;

    @Override
    public BaseResult selectActivityList(Integer page,Integer rows) {
        BaseResult baseResult = new BaseResult();
        Map<String, Object> param = new HashMap<>();
        Integer start = null;
        if (page != null && rows != null && page > 0) {
            start = page != null ? (page - 1) * rows : null;
            param.put("page", start);
            param.put("rows", rows);
        }
        //将库里数据设置【进行中】
        ylActivityMapper.updateByTime();
        // 将库里数据设置【已结束】
        ylActivityMapper.updateByTime1();
        // 查询所有的医疗活动信息
        List<YlActivity> ylActivities = ylActivityMapper.selectYlActivityList(param);
        // 查询总数
        int count = ylActivityMapper.selectActivityCount();

        baseResult.setData(ylActivities);
        baseResult.setTotal(count);
        baseResult.setRows(rows);
        baseResult.setSuccess(true);
        return baseResult;
    }

    @Override
    public BaseResult selectMyActivityList(Integer uid,String userName, Integer page, Integer rows) {
        BaseResult baseResult = new BaseResult();
        Map<String, Object> param = new HashMap<>();
        Integer start = null;
        if (page != null && rows != null && page > 0) {
            start = page != null ? (page - 1) * rows : null;
            param.put("page", start);
            param.put("rows", rows);
        }
        param.put("userName",userName);
        // 根据uid去查询医疗活动结果
        List<YlRecord> ylRecords = ylRecordMapper.selectYlRecordList(param);
        // 查询总数
        int count = ylRecordMapper.selectYlRecordCount(param);
        baseResult.setData(ylRecords);
        baseResult.setTotal(count);
        baseResult.setRows(rows);
        baseResult.setSuccess(true);
        return baseResult;
    }
}
