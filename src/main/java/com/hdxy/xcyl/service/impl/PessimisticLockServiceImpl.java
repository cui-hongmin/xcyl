package com.hdxy.xcyl.service.impl;

import com.hdxy.xcyl.common.utils.BaseResult;
import com.hdxy.xcyl.entity.Order;
import com.hdxy.xcyl.entity.Product;
import com.hdxy.xcyl.mapper.OrderMapper;
import com.hdxy.xcyl.mapper.ProductMapper;
import com.hdxy.xcyl.service.PessimisticLockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

@Service
public class PessimisticLockServiceImpl implements PessimisticLockService {

    private double[] accounts;
    private Lock lock = new ReentrantLock();

    @Autowired
    private ProductMapper productMapper;
    @Autowired
    private OrderMapper orderMapper;


    /**
     * 并发量少  -- 乐观锁
     * */
    @Override
    public BaseResult insertOrder(Integer id, BigDecimal purchaseNumber) {
        Product product = productMapper.selectProductById(id);
        if (product.getPurchaseRedNumber().compareTo(purchaseNumber) >= 0) {
            System.out.println("扣减剩余量");
            // 扣减剩余量
            int count = productMapper.updateProductResidue(id, purchaseNumber);
            if (count > 0) {
                System.out.println("开始下单");
                Order order = new Order();
                //生成订单编号
                SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
                String newDate = sdf.format(new Date());
                String resul = "";
                Random random = new Random();
                for (int i = 0; i < 2; i++) {
                    resul += random.nextInt(6);
                }
                String orderNo = "DD" + newDate + resul;
                order.setOrderNo(orderNo);
                order.setUid(9206);
                order.setPurchaseNumber(purchaseNumber);
                order.setCreateTime(new Date());
                int i = orderMapper.insertOrder(order);
                if (i > 0) {
                    System.out.println("下单成功");
                    return BaseResult.success("下单成功");
                }else {
                    System.out.println("下单失败");
                    return BaseResult.fail("502","下单失败");
                }


            }else {
                System.out.println("更新剩余量失败");
                return BaseResult.fail("502","更新剩余量失败");
            }


        }
        System.out.println("剩余量不足");
        return BaseResult.fail("502","该商品剩余量不足");
    }

    /**
     * 并发量大  -- 悲观锁
     * */
    @Override
    public BaseResult insertOrder1(Integer id, BigDecimal purchaseNumber) {

        Product product = productMapper.selectProductById(id);
        if (product.getPurchaseRedNumber().compareTo(purchaseNumber) >= 0) {
            System.out.println("获取到锁");
            System.out.println("扣减剩余量");

            // 扣减剩余量
            int count = productMapper.updateProductResidue1(id, purchaseNumber);
            if (count > 0) {
                System.out.println("开始下单");
                Order order = new Order();
                //生成订单编号
                SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
                String newDate = sdf.format(new Date());
                String resul = "";
                Random random = new Random();
                for (int i = 0; i < 2; i++) {
                    resul += random.nextInt(6);
                }
                String orderNo = "DD" + newDate + resul;
                order.setOrderNo(orderNo);
                order.setUid(9206);
                order.setPurchaseNumber(purchaseNumber);
                order.setCreateTime(new Date());

                int i = orderMapper.insertOrder(order);
                if (i > 0) {
                    System.out.println("下单成功");
                    return BaseResult.success("下单成功");
                }else {
                    System.out.println("下单失败");
                    return BaseResult.fail("502","下单失败");
                }
            }else {
                System.out.println("更新剩余量失败");
                return BaseResult.fail("502","更新剩余量失败");
            }
        }
        System.out.println("剩余量不足");
        return BaseResult.fail("502","该商品剩余量不足");
    }
}
