package com.hdxy.xcyl.service.impl;

import com.hdxy.xcyl.common.utils.BaseResult;
import com.hdxy.xcyl.entity.YlDoctor;
import com.hdxy.xcyl.entity.YlInfusion;
import com.hdxy.xcyl.entity.YlSymptoms;
import com.hdxy.xcyl.mapper.YlDoctorMapper;
import com.hdxy.xcyl.mapper.YlInfusionMapper;
import com.hdxy.xcyl.mapper.YlSymptomsMapper;
import com.hdxy.xcyl.service.YlDoctorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class YlDoctorServiceImpl implements YlDoctorService {

    @Autowired
    private YlDoctorMapper ylDoctorMapper;
    @Autowired
    private YlSymptomsMapper ylSymptomsMapper;
    @Autowired
    private YlInfusionMapper ylInfusionMapper;

    @Override
    public BaseResult insertConsultation(Integer uid, String ysName, Integer doctorNumber, String userName, String userSymptoms) {
        BaseResult base = new BaseResult();
        YlSymptoms ylSymptoms = new YlSymptoms();
        ylSymptoms.setUid(uid);
        ylSymptoms.setYsName(ysName);
        ylSymptoms.setDoctorNumber(doctorNumber);
        ylSymptoms.setUserName(userName);
        ylSymptoms.setUserSymptoms(userSymptoms);
        ylSymptoms.setStatus(0);
        Boolean i = ylSymptomsMapper.insertYlSymptoms(ylSymptoms) > 0;
//        int count = shopOrderMapper.insertShopOrder(shopOrder);
        if (i) {
            return BaseResult.success();
        }
        return BaseResult.fail("502","新增数据失败");
    }

    @Override
    public BaseResult insertInfusion(Integer uid, String userName, Integer userAge, Integer userSex, String userMobile, String userAddress, String userDescribe) {
        YlInfusion ylInfusion = new YlInfusion();
        // 确定余量
        YlInfusion ylInfusion1 = ylInfusionMapper.selectYlInfusionNow();
        if (ylInfusion1 == null) {
            ylInfusion.setMargin(19);
        }else if (ylInfusion1.getMargin() == 0){
            return BaseResult.fail("502","今日线上预约余量已不足，请明日再次预约！");
        }else {
            ylInfusion.setMargin(ylInfusion1.getMargin() - 1);
        }
        ylInfusion.setUid(uid);
        ylInfusion.setUserName(userName);
        ylInfusion.setUserAge(userAge);
        ylInfusion.setUserSex(userSex);
        ylInfusion.setUserMobile(userMobile);
        ylInfusion.setUserAddress(userAddress);
        ylInfusion.setUserDescribe(userDescribe);
        ylInfusion.setStatus(0);

        boolean i = ylInfusionMapper.insertYlInfusion(ylInfusion) > 0;
        if (i) {
            return BaseResult.success();
        }
        return BaseResult.fail("502","新增数据失败");
    }

    @Override
    public BaseResult selectDoctorById(Integer id) {
        return null;
    }

    @Override
    public BaseResult selectDoctorList() {
        List<YlDoctor> ylDoctors = ylDoctorMapper.selectYlDoctorList();
        return BaseResult.success(ylDoctors);
    }

    @Override
    public BaseResult selectSymptomsList(Integer uid) {
        List<YlSymptoms> ylSymptoms = ylSymptomsMapper.selectSymptomsList(uid);
        return BaseResult.success(ylSymptoms);
    }
}
