package com.hdxy.xcyl.service.impl;

import com.hdxy.xcyl.common.exception.ResultException;
import com.hdxy.xcyl.common.utils.*;
import com.hdxy.xcyl.entity.User;
import com.hdxy.xcyl.entity.UserLoginLogs;
import com.hdxy.xcyl.mapper.UserMapper;
import com.hdxy.xcyl.service.SmsService;
import com.hdxy.xcyl.service.UserService;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class UserServiceImpl implements UserService {

    private static final long time = -1;
    private static String pattern = "^(?![A-Za-z0-9]+$)(?![a-z0-9_\\W]+$)(?![A-Za-z_\\W]+$)(?![A-Z0-9_\\W]+$)[a-zA-Z0-9_\\W]{8,36}$";

    @Autowired
    RedisUtil redisUtil;

    @Autowired
    private UserMapper userMapper;
    @Autowired
    private SmsService smsService;

    @Transactional
    @Override
    public BaseResult register(String mobile, String password, String code, String userName) {
        //密码正则匹配
        Pattern complier = Pattern.compile(pattern);
        Matcher matcher = complier.matcher(password);
        if (!matcher.find()){
            return BaseResult.fail("502","密码格式不正确,请重新输入!");
        }
        User newUser = new User();
        // 验证
        if (!CommonUtils.isMobile(mobile)) {
            return BaseResult.fail("501", "电话号码格式不正确");
        }
        // 验证码校验
        BaseResult baseResult = smsService.checkCode(mobile, "register", code);
        if (!baseResult.isSuccess()) {
            return baseResult;
        }
        // 用户名是否存在
        User user1 = userMapper.selectUserByUserName(userName);
        if ( user1 != null) {
            return BaseResult.fail("502", "用户名已注册");
        }
        // 手机号码是否存在
        User user = userMapper.selectUserByMobile(mobile);
        int count = 0;
        if(user != null){
            return BaseResult.fail("502", "电话号码已注册");
        } else{
            // 注册入库
            newUser.setPhone(mobile);
            newUser.setPassword(MD5Util.MD5(password));
            newUser.setUserName(userName);

            count = userMapper.insertUser(newUser);
        }
        if(count==0){
            return BaseResult.fail("502", "注册失败");
        }
        // 注册成功 保存token
        String key = "user_"+newUser.getUid();
        String token = StringUtil.get32UUID();
        redisUtil.set(key, token,time);
        newUser.setToken(token);
        newUser.setPassword(null);
        return BaseResult.success(newUser);
    }

    @Override
    public BaseResult loginApp(String userName, String password, String ip, String sessionId) {
        // 判断用户是否存在
        // user是否存在
        User user = userMapper.selectUserByUserName(userName);
        if (user == null) {
            return BaseResult.fail("502", "用户不存在,请先注册。");
        }
        if (user.getPassword()==null){
            return BaseResult.fail("502", "您未设置密码,请设置密码!");
        }
        if (!user.getPassword().equals(MD5Util.MD5(password))) {
            return BaseResult.fail("502", "密码不正确");
        }
        //验证通过 保存token 返回 登录成功
        String key = "user_"+user.getUid();
        String token = (String)redisUtil.get(key);
        if(token==null || "".equals(token)){
            token = StringUtil.get32UUID();
            boolean is = redisUtil.set(key, token,time);
            if(!is) {
                return BaseResult.fail("502", "登录失败");
            }
        }
        user.setToken(token);

        // 记录UserLoginlogs
        UserLoginLogs userLoginlogs = new UserLoginLogs();
        userLoginlogs.setUid(user.getUid());
        userLoginlogs.setLoginIP(ip);
//        userLoginlogs.setChannel(channel);
        userLoginlogs.setSessionId(sessionId);
        int loginCount = userMapper.insertUserLoginLogs(userLoginlogs);
        if (loginCount <= 0) {
            return BaseResult.fail("502", "更新登录日志失败");
        }
        user.setPassword(null);
        return BaseResult.success(user);
    }

    @Override
    public BaseResult checkUserAuth(String token, String uid) {
        String key = "user_"+uid;
        if (!StringUtil.checkNotNull(token)) {
            return BaseResult.fail("600", "token can not be empty");
        }
        if (!StringUtil.checkNotNull(uid)) {
            return BaseResult.fail("600", "uid can not be empty");
        }
        String redisToken = (String)redisUtil.get(key);  // 到redis中获取token
        if (!StringUtil.checkNotNull(redisToken)) {
            return BaseResult.fail("600", "login timeout");
        }
        if(!token.equals(redisToken)) {
            return BaseResult.fail("600", "login timeout");
        }
        //TODO 更新token数据
        if(time<=0){
            boolean is = redisUtil.set(key, token,time);
            if(is){
                return BaseResult.success();
            }
        }
        return BaseResult.success();
    }

    @Override
    public BaseResult updateUser(Integer uid, String userName, String newPassword, String phone) {

        // 根据uid查询客户 判断用户是否存在
        User user = userMapper.selectUserByUid(uid);
        if (user == null) {
            return BaseResult.fail("502", "非法用户，请先注册。");
        }
        User userMap = new User();
        if (newPassword != null && !newPassword.equals("")) {
            //密码正则匹配
            Pattern complier = Pattern.compile(pattern);
            Matcher matcher = complier.matcher(newPassword);
            if (!matcher.find()){
                return BaseResult.fail("502","密码格式不正确,请重新输入!");
            }
            userMap.setPassword(MD5Util.MD5(newPassword));
        }

        userMap.setUid(uid);
        userMap.setUserName(userName);
        userMap.setPhone(phone);
        // 根据用户uid修改用户信息
        int count = userMapper.updateUserById(userMap);
        if (count <= 0) {
            throw new ResultException("502", "修改用户信息失败!");
        }
        // 根据用户uid获取修改后的用户信息
        User resultUser = userMapper.selectUserByUid(uid);
        resultUser.setPassword(null);
        return BaseResult.success(resultUser);
    }

    @Override
    @Transactional
    public void createPDF() throws FileNotFoundException, DocumentException {
        // 1.新建document对象
        Document document = new Document();

        // 2.建立一个书写器(Writer)与document对象关联，通过书写器(Writer)可以将文档写入到磁盘中。
        // 创建 PdfWriter 对象 第一个参数是对文档对象的引用，第二个参数是文件的实际名称，在该名称中还会给出其输出路径。
//        PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream("E:/ks//aaa/test.pdf"));

        // 3.打开文档
        document.open();

        // 4.添加一个内容段落
        document.add(new Paragraph("Hello World!"));

        // 5.关闭文档
        document.close();


    }
}
