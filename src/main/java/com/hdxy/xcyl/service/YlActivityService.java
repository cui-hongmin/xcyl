package com.hdxy.xcyl.service;

import com.hdxy.xcyl.common.utils.BaseResult;
import io.swagger.models.auth.In;

public interface YlActivityService {

    /**
     * 获取医疗活动信息列表
     *
     * @return
     */
    BaseResult selectActivityList(Integer page,Integer rows);

    /**
     * 根据uid获取我的医疗活动结果列表
     *
     * @return
     */
    BaseResult selectMyActivityList(Integer uid,String userName,Integer page,Integer rows);
}
