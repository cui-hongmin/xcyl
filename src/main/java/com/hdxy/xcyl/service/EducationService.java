package com.hdxy.xcyl.service;

import com.hdxy.xcyl.common.utils.BaseResult;

public interface EducationService {

    /**
     * 获取预防性教育标题信息
     *
     * @return
     */
    BaseResult selectEducation(Integer type);

    /**
     * 根据id获取预防性教育(国家政策指南 防疫常识)详细信息
     *
     * @return
     */
    BaseResult selectEducationById(Integer id);

    /**
     * 根据id获取预防性教育(疫苗接种服务)详细信息
     *
     * @return
     */
    BaseResult selectYlSeekById(Integer id);
}
