package com.hdxy.xcyl.service;

import com.hdxy.xcyl.common.utils.BaseResult;

import java.math.BigDecimal;

public interface PessimisticLockService {

    // 验证乐观锁
    BaseResult insertOrder(Integer id, BigDecimal purchaseNumber);

    // 验证悲观锁
    BaseResult insertOrder1(Integer id, BigDecimal purchaseNumber);
}
