package com.hdxy.xcyl.service;

import com.hdxy.xcyl.common.utils.BaseResult;
import com.itextpdf.text.DocumentException;

import java.io.FileNotFoundException;

public interface UserService {

    /**
     * 登录
     * @param userName  用户名
     * @param password  密码
     * @param ip
     * @return
     */
    BaseResult loginApp(String userName, String password, String ip, String sessionId);

    /**
     * 注册
     * @param mobile  手机号
     * @param password 密码
     * @param code  验证码
     * @param userName  用户名
     * @return
     */
    BaseResult register( String mobile, String password, String code, String userName);

    /**
     * 校验登录token是否过期
     * @param token
     * @param uid
     * @return
     */
    BaseResult checkUserAuth(String token,String uid);

    /**
     * 修改用户信息
     * @param uid  用户uid
     * @param userName  用户名
     * @param newPassword  新密码
     * @param phone  电话号码
     * @return
     */
    BaseResult updateUser(Integer uid,String userName, String newPassword,String phone);


    void createPDF() throws FileNotFoundException, DocumentException;
}
