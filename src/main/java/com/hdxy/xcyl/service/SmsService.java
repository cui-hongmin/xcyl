package com.hdxy.xcyl.service;

import com.hdxy.xcyl.common.utils.BaseResult;

public interface SmsService {
    /**
     * 校验手机号
     * @param mobile
     * @param tmpKey  模板key
     * @param code
     * @return
     * */
    public BaseResult checkCode(String mobile, String tmpKey, String code);

    /**
     * 生成验证码
     *
     */
    public BaseResult sendSmsWithCode();

    /**
     * 获取验证码开关
     *
     * @return true开启，false关闭
     */
//    public boolean selectCaptchaEnabled();
}
