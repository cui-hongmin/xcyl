package com.hdxy.xcyl.service;

import com.hdxy.xcyl.common.utils.BaseResult;

public interface YLVillageService {

    BaseResult insertVillage( String ylName, String phone, String isCare, String ylIllnessOne,String ylIllnessTwo,String ylVillageOne,String ylVillageTwo);
}
