package com.hdxy.xcyl.service;

import com.hdxy.xcyl.common.utils.BaseResult;

public interface YlDoctorService {

    /**
     * 线上问诊提交
     * */
    BaseResult insertConsultation(Integer uid, String ysName,Integer doctorNumber, String userName, String userSymptoms);

    /**
     * 线上预约输液提交
     * */
    BaseResult insertInfusion( Integer uid, String userName, Integer userAge,Integer userSex, String userMobile, String userAddress, String userDescribe);

    /**
     * 根据id获取医生详细信息
     *
     * @return
     */
    BaseResult selectDoctorById(Integer id);

    /**
     * 获取医生信息列表
     *
     * @return
     */
    BaseResult selectDoctorList();

    /**
     * 获取问诊查询信息列表
     *
     * @return
     */
    BaseResult selectSymptomsList(Integer uid);





}
