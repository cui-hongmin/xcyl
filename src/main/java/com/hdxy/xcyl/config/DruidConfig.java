package com.hdxy.xcyl.config;


import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.druid.support.http.StatViewServlet;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;
import java.util.HashMap;

/**
 * druid 配置多数据源
 *
 * @author ruoyi
 */
@Configuration
public class DruidConfig {

    //绑定application.yaml
    @ConfigurationProperties(prefix = "spring.datasource")
    @Bean
    public DataSource dataSource(){
        //将自定义的容器DruidDataSource加载到容器中之后，就不用SpringBoot来创建了
        return new DruidDataSource();
    }

    @Bean
    //后台监控
    public ServletRegistrationBean statViewServlet(){
        ServletRegistrationBean<StatViewServlet> bean = new ServletRegistrationBean<>(new StatViewServlet(),"/druid/*");//  /druid/*路径
        //后台需要有人登录，账号吗，密码设置
        HashMap<String,String> initParameters = new HashMap<>();
        //增加配置
        //注意：登录key是固定形式，不可以修改
        initParameters.put("loginUsername","admin");
        initParameters.put("loginPassword","12345");

        //允许谁访问,就在写在allow后例如  initParameters.put("allow","localhost")：只允许主机访问
        initParameters.put("allow","");
        //禁止谁访问
        //initParameters.put("GAO","192.168.65.2");
        //设置初始化参数
        bean.setInitParameters(initParameters);
        return bean;


    }

}
