package com.hdxy.xcyl.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class YLIllne implements Serializable {
    private Integer id;

    private String areaCode;

    private String areaName;

    private Integer pid;

    private String createBy;

    private Date createTime;

    private List<YLIllne> children;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAreaCode() {
        return areaCode;
    }

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public Integer getPid() {
        return pid;
    }

    public void setPid(Integer pid) {
        this.pid = pid;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public List<YLIllne> getChildren() {
        return children;
    }

    public void setChildren(List<YLIllne> children) {
        this.children = children;
    }
}
