package com.hdxy.xcyl.entity;

import java.io.Serializable;
import java.util.Date;

public class YLVillage implements Serializable {

    private Integer id;
    private String ylName;
    private String phone;
    private String isCare;
    private String ylIllnessOne;
    private String ylIllnessTwo;
    private String ylVillageOne;
    private String ylVillageTwo;
    private String createBy;
    private Date createTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getYlName() {
        return ylName;
    }

    public void setYlName(String ylName) {
        this.ylName = ylName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getIsCare() {
        return isCare;
    }

    public void setIsCare(String isCare) {
        this.isCare = isCare;
    }

    public String getYlIllnessOne() {
        return ylIllnessOne;
    }

    public void setYlIllnessOne(String ylIllnessOne) {
        this.ylIllnessOne = ylIllnessOne;
    }

    public String getYlIllnessTwo() {
        return ylIllnessTwo;
    }

    public void setYlIllnessTwo(String ylIllnessTwo) {
        this.ylIllnessTwo = ylIllnessTwo;
    }

    public String getYlVillageOne() {
        return ylVillageOne;
    }

    public void setYlVillageOne(String ylVillageOne) {
        this.ylVillageOne = ylVillageOne;
    }

    public String getYlVillageTwo() {
        return ylVillageTwo;
    }

    public void setYlVillageTwo(String ylVillageTwo) {
        this.ylVillageTwo = ylVillageTwo;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}
