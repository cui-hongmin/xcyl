package com.hdxy.xcyl.entity;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

public class YlActivity implements Serializable {

    /**
     * 主键
     */
    private Integer id;
    /**
     * 医疗活动名称
     */
    private String ylName;
    /**
     * 医疗活动地址
     */
    private String ylAddress;
    /**
     * 状态（0：未开始；1：进行中；2：已结束）
     */
    private Integer status;

    private String statusStr;
    /**
     * 医疗活动开始时间
     */
    private Date ylStartTime;

    private String ylStartTimeStr;
    /**
     * 医疗活动截至时间
     */
    private Date ylEndTime;

    private String ylEndTimeStr;
    /**
     * 删除标志（0代表存在 2代表删除）
     */
    private String delFlag;

    private Date createTime;

    public String getStatusStr() {
        if(this.status == 0){
            statusStr = "未开始";
        }else if(this.status == 1){
            statusStr = "进行中";
        }else if(this.status == 2){
            statusStr = "已结束";
        }
        return statusStr;
    }

    public String getYlStartTimeStr() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        if(this.ylStartTime!=null){
            ylStartTimeStr = sdf.format(this.ylStartTime);
        }
        return ylStartTimeStr;
    }

    public String getYlEndTimeStr() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        if(this.ylEndTime!=null){
            ylEndTimeStr = sdf.format(this.ylEndTime);
        }
        return ylEndTimeStr;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getYlName() {
        return ylName;
    }

    public void setYlName(String ylName) {
        this.ylName = ylName;
    }

    public String getYlAddress() {
        return ylAddress;
    }

    public void setYlAddress(String ylAddress) {
        this.ylAddress = ylAddress;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Date getYlStartTime() {
        return ylStartTime;
    }

    public void setYlStartTime(Date ylStartTime) {
        this.ylStartTime = ylStartTime;
    }

    public Date getYlEndTime() {
        return ylEndTime;
    }

    public void setYlEndTime(Date ylEndTime) {
        this.ylEndTime = ylEndTime;
    }

    public String getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}
