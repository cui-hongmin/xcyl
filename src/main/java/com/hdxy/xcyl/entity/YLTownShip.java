package com.hdxy.xcyl.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class YLTownShip implements Serializable {

    private Integer id;

    private String ylCode;

    private String ylName;

    private Integer pid;

    private String createBy;

    private Date createTime;

    private List<YLTownShip> children;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getYlCode() {
        return ylCode;
    }

    public void setYlCode(String ylCode) {
        this.ylCode = ylCode;
    }

    public String getYlName() {
        return ylName;
    }

    public void setYlName(String ylName) {
        this.ylName = ylName;
    }

    public Integer getPid() {
        return pid;
    }

    public void setPid(Integer pid) {
        this.pid = pid;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public List<YLTownShip> getChildren() {
        return children;
    }

    public void setChildren(List<YLTownShip> children) {
        this.children = children;
    }
}
