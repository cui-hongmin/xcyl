package com.hdxy.xcyl.entity;

import io.swagger.models.auth.In;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

public class YlSeek implements Serializable {
    private Integer id;

    private String title;

    private String ylUrl;

    private String ylDescribe;

    private String createBy;

    private Date createTime;

    private String updateBy;

    private Date updateTime;

    private String remark;

    private Integer delFlag;

    private String createTimeStr;

    public String getCreateTimeStr() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        if(this.createTime!=null){
            createTimeStr = sdf.format(this.createTime);
        }
        return createTimeStr;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getYlUrl() {
        return ylUrl;
    }

    public void setYlUrl(String ylUrl) {
        this.ylUrl = ylUrl;
    }

    public String getYlDescribe() {
        return ylDescribe;
    }

    public void setYlDescribe(String ylDescribe) {
        this.ylDescribe = ylDescribe;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Integer getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(Integer delFlag) {
        this.delFlag = delFlag;
    }
}
