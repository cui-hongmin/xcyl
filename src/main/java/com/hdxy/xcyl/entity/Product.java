package com.hdxy.xcyl.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class Product implements Serializable {
    private Integer id;

    private String productName;

    private BigDecimal purchaseTotalNumber; // 总量

    private BigDecimal purchaseRedNumber;  // 剩余量

    private Date createTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }


    public BigDecimal getPurchaseTotalNumber() {
        return purchaseTotalNumber;
    }

    public void setPurchaseTotalNumber(BigDecimal purchaseTotalNumber) {
        this.purchaseTotalNumber = purchaseTotalNumber;
    }

    public BigDecimal getPurchaseRedNumber() {
        return purchaseRedNumber;
    }

    public void setPurchaseRedNumber(BigDecimal purchaseRedNumber) {
        this.purchaseRedNumber = purchaseRedNumber;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}
