package com.hdxy.xcyl.entity;

import io.swagger.models.auth.In;

import javax.xml.crypto.Data;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class Order implements Serializable {

    private Integer id;

    private Integer uid;

    private String orderNo;

    private BigDecimal purchaseNumber;

    private Date createTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public BigDecimal getPurchaseNumber() {
        return purchaseNumber;
    }

    public void setPurchaseNumber(BigDecimal purchaseNumber) {
        this.purchaseNumber = purchaseNumber;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}
