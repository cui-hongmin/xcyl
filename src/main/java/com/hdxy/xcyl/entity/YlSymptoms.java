package com.hdxy.xcyl.entity;

import io.swagger.models.auth.In;

import java.io.Serializable;
import java.util.Date;

public class YlSymptoms implements Serializable {

    private Integer id;

    private Integer uid;

    private String ysName;

    private Integer doctorNumber;

    private String userName;

    private String userSymptoms;

    private Integer status;

    private String ysSymptoms;

    private Date createTime;

    private Date updateTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public String getYsName() {
        return ysName;
    }

    public void setYsName(String ysName) {
        this.ysName = ysName;
    }

    public Integer getDoctorNumber() {
        return doctorNumber;
    }

    public void setDoctorNumber(Integer doctorNumber) {
        this.doctorNumber = doctorNumber;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserSymptoms() {
        return userSymptoms;
    }

    public void setUserSymptoms(String userSymptoms) {
        this.userSymptoms = userSymptoms;
    }

    public Integer getStatus() {
        return status;
    }

    public String getYsSymptoms() {
        return ysSymptoms;
    }

    public void setYsSymptoms(String ysSymptoms) {
        this.ysSymptoms = ysSymptoms;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}
