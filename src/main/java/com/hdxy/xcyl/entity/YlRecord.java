package com.hdxy.xcyl.entity;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

public class YlRecord implements Serializable {
    /**
     * 主键
     */
    private Integer id;

    /**
     * 医疗活动id
     */
    private Integer activityId;

    /**
     * 医疗活动名称
     */
    private String ylName;

    /**
     * 用户名
     */
    private String userName;

    /**
     * 姓名
     */
    private String xxName;
    /**
     * 年龄
     */
    private Integer xxAge;
    /**
     * 性别
     */
    private String xxSex;
    /**
     * 电话
     */
    private String xxMobile;
    /**
     * 详细地址
     */
    private String xxAddress;

    /**
     * 病情描述
     */
    private String xxDescribe;

    /**
     * 状态
     */
    private Integer status;

    /**
     * 是否通知结果（0：不通知，1通知）
     */
    private Integer isNotice;

    /**
     * 结果
     */
    private String result;

    /**
     * 检测机构
     */
    private String procAgency;

    /**
     * 检测时间
     */
    private Date procTime;

    private String procTimeStr;

    private Date createTime;

    /**
     * 备注
     */
    private String remark;
    /**
     * 删除标志（0代表存在 2代表删除）
     */
    private String delFlag;

    private String createTimeStr;

    public String getCreateTimeStr() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        if(this.createTime!=null){
            createTimeStr = sdf.format(this.createTime);
        }
        return createTimeStr;
    }

    public String getProcTimeStr() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        if(this.procTime!=null){
            procTimeStr = sdf.format(this.procTime);
        }
        return procTimeStr;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getActivityId() {
        return activityId;
    }

    public void setActivityId(Integer activityId) {
        this.activityId = activityId;
    }

    public String getYlName() {
        return ylName;
    }

    public void setYlName(String ylName) {
        this.ylName = ylName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getXxName() {
        return xxName;
    }

    public void setXxName(String xxName) {
        this.xxName = xxName;
    }

    public Integer getXxAge() {
        return xxAge;
    }

    public void setXxAge(Integer xxAge) {
        this.xxAge = xxAge;
    }

    public String getXxSex() {
        return xxSex;
    }

    public void setXxSex(String xxSex) {
        this.xxSex = xxSex;
    }

    public String getXxMobile() {
        return xxMobile;
    }

    public void setXxMobile(String xxMobile) {
        this.xxMobile = xxMobile;
    }

    public String getXxAddress() {
        return xxAddress;
    }

    public void setXxAddress(String xxAddress) {
        this.xxAddress = xxAddress;
    }

    public String getXxDescribe() {
        return xxDescribe;
    }

    public void setXxDescribe(String xxDescribe) {
        this.xxDescribe = xxDescribe;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getIsNotice() {
        return isNotice;
    }

    public void setIsNotice(Integer isNotice) {
        this.isNotice = isNotice;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getProcAgency() {
        return procAgency;
    }

    public void setProcAgency(String procAgency) {
        this.procAgency = procAgency;
    }

    public Date getProcTime() {
        return procTime;
    }

    public void setProcTime(Date procTime) {
        this.procTime = procTime;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag;
    }

}
